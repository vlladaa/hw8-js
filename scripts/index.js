/*
--- теорія ---

1) DOM - структуризація сторінки у вигляді дерева об'єктів, що складається з елементів та вузлів
2) різниця полягає в тому що innerHTML зчитує теги HTML як теги, а не як текст і символи, тому перетворює їх на сторінці 
3) це можно зробити багатьма способами в залежнсті від того що ми маємо чи через що хотіли б звернутися.
   document.getElementById() (за айді),  document.getElementByClassName() (за класом), document.getElementByTagName() (зі тегом).
   також можна звернутися через  document.querySelector() (для 1 селектору) чи document.querySelectorAll() (для всіх). краще використовувати ці способи,
   бо вони більш універсальні та приймають і теги, і класи, і айді

*/

//1
const p = document.querySelectorAll("p");
p.forEach((p) => {
  p.style.background = "#ff0000";
})
//2
const elementId = document.querySelector("#optionsList");
console.log(elementId);
const parent = elementId.parentElement;
console.log(parent)
const childNodes = elementId.childNodes;
childNodes.forEach((childNodes) => {
  console.log(`${childNodes.nodeName} - ${childNodes.nodeType}`)
}) 
//3
const content = document.querySelector("#testParagraph");
content.textContent = "This is a paragraph";
//4
const li = document.querySelectorAll(".main-header li");
console.log(li);
li.forEach((li) => {
  li.classList.add('nav-item')
})
//5
const deleteElem = document.querySelectorAll(".section-title")
deleteElem.forEach((elem) => {
  elem.remove()
})